const FormData = require("form-data");
import axios from "axios";
import * as fs from "fs";

const BITBUCKET_REPO_OWNER = process.env.BITBUCKET_REPO_OWNER;
const BITBUCKET_REPO_SLUG = process.env.BITBUCKET_REPO_SLUG;
const BB_AUTH_STRING = process.env.BB_AUTH_STRING || "";
const TITLE = process.env.TITLE;
const DESCRIPTION = process.env.DESCRIPTION;
const SOURCE_BRANCH = process.env.SOURCE_BRANCH;
const DESTINATION_BRANCH = process.env.DESTINATION_BRANCH;
const KEYWORD = process.env.KEYWORD;
const HEAD_COMMIT = process.env.HEAD_COMMIT;

const GetBitbucketToken = async (): Promise<string> => {
  const basicAuth = "Basic " + Buffer.from(BB_AUTH_STRING).toString("base64");
  let data = new FormData();
  data.append("grant_type", "client_credentials");
  data.append("scopes", "repository");
  const result = await axios.post(
    "https://bitbucket.org/site/oauth2/access_token",
    data,
    { headers: { Authorization: basicAuth } }
  );
  return result.data.access_token;
};

const CreatePullRequest = async (token: string): Promise<any> => {
  const body = {
    title: TITLE,
    description: DESCRIPTION,
    source: {
      branch: {
        name: SOURCE_BRANCH,
      },
    },
    destination: {
      branch: {
        name: DESTINATION_BRANCH,
      },
    },
    close_source_branch: false,
  };

  const result = await axios.post(
    `https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/pullrequests`,
    body,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );

  return result;
};

const run = async () => {
  try {
    const token = await GetBitbucketToken();
    if (!KEYWORD || HEAD_COMMIT?.includes(KEYWORD)) {
      const result = await CreatePullRequest(token);
      // adding output variables in the env file
      fs.writeFile(
        ".env",
        `PULL_REQUEST_ID=${result?.data?.id}`,
        (err: any) => {
          if (err) throw err;
          console.log("The file has been saved!");
        }
      );
      return result;
    }
    if (!HEAD_COMMIT?.includes(KEYWORD)) {
      fs.writeFile(".env", ``, (err: any) => {
        if (err) throw err;
        console.log("The file has been saved!");
      });
      return;
    }
  } catch (error: any) {
    console.log("error", error?.message);
    if (BB_AUTH_STRING) {
      throw error;
    }
  }
};

run();
