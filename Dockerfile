FROM node:18-alpine3.14


RUN apk add --update --no-cache bash

COPY pipe .
COPY LICENSE.txt pipe.yml README.md /

RUN npm install -g typescript

RUN npm install

RUN npm run build

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
