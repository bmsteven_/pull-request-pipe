# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.12

- patch: Fix security vulnerability
- patch: Fix security vulnerability

## 0.1.11

- patch: Fix security vulnerability

## 0.1.10

- patch: Fix security vulnerability
- patch: Fix security vulnerability

## 0.1.9

- patch: Fix security vulnerability
- patch: Fix security vulnerability

## 0.1.8

- patch: Fix security vulnerability

## 0.1.7

- patch: Fix security vulnerability

## 0.1.6

- patch: Fix security vulnerability

## 0.1.5

- patch: Fix security vulnerability

## 0.1.4

- patch: Fix security vulnerability
- patch: Fix security vulnerability

## 0.1.3

- patch: Fix security vulnerability.

## 0.1.2

- patch: Fix security vulnerability with authentication.

## 0.1.1

- patch: Fix security vulnerability with authentication.

## 0.1.0

- minor: Initial release

