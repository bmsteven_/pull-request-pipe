# Bitbucket Pipelines Pipe: Pull Request Automations

This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: bmsteven_/automate-pr-creation:0.1.12
    variables:
      BITBUCKET_REPO_OWNER: "<string>"
      BITBUCKET_REPO_SLUG: "<string>"
      SOURCE_BRANCH: "<string>"
      DESTINATION_BRANCH: "<string>"
      TITLE: "<string>"
      DESCRIPTION: "<string>"
      REVIEWERS: "<array>"
      # DEBUG: "<boolean>" # Optional
```

## Variables

| Variable  | Usage                                              |
| --------- | -------------------------------------------------- |
| NAME (\*) | The name that will be printed in the logs          |
| DEBUG     | Turn on extra debug information. Default: `false`. |

_(\*) = required variable._

## Prerequisites

## Examples

Basic example:

```yaml
script:
  - pipe: bmsteven_/automate-pr-creation:0.1.12
    variables:
      variables:
      BITBUCKET_REPO_OWNER: "<string>"
      BITBUCKET_REPO_SLUG: "<string>"
      SOURCE_BRANCH: "<string>"
      DESTINATION_BRANCH: "<string>"
      TITLE: "<string>"
      DESCRIPTION: "<string>"
      REVIEWERS: "<array>"
```

Advanced example:

```yaml
script:
  - pipe: bmsteven_/automate-pr-creation:0.1.12
    variables:
      NAME: "foobar"
      DEBUG: "true"
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by bmsteve@yahoo.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
